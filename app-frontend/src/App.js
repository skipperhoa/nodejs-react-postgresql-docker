import logo from './logo.svg';
import './App.css';
import React,{useState,useEffect} from 'react';
const App = ()=>{
  const [tutotials,setTutotials]=useState([]);
  const [title,setTitile] = useState("");
  const [description,setDescription] = useState("")
  const [update,setUpdate] = useState(false);
  const [id,setId] = useState(0);
  useEffect(()=>{
    fetch("http://localhost:8080/api/tutorials").then((res)=>res.json())
    .then(result=>setTutotials(result));

  },[tutotials]);
  const insertTutotial = ()=>{
      var data = {
         title:title,
         description:description,
         published:true
      }
      var config = {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }
      fetch("http://localhost:8080/api/tutorials",config).then((res)=>res.json())
      .then(result=>console.log(result));
  }

  const deleteTutotial = (id)=>{
    var config = {
      method: 'delete'
    }
    fetch("http://localhost:8080/api/tutorials/"+id,config).then((res)=>res.json())
    .then(result=>console.log(result));
  }

  const editTutotial = (id) => {
    
    fetch("http://localhost:8080/api/tutorials/"+id).then((res)=>res.json())
    .then(result=>{
      setId(result.id)
       setTitile(result.title);
       setDescription(result.description);
       setUpdate(true);
    });
  };

  const updateTutotial=()=>{
      var data = {
          title:title,
          description:description,
          published:true
      }
      var config = {
        method: 'put',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    }
      fetch("http://localhost:8080/api/tutorials/"+id,config).then((res)=>res.json())
      .then(result=>setUpdate(false));
  }
  

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React - Hoanguyenit.com
        </a>
      </header>
      <div className="post_tutitias">
       <h5>Insert Tutotials</h5>
        <label>Title</label>
        <input type="text" name="title" onChange = {(e)=>setTitile(e.target.value)} value={title}/>
        <label>Description</label>
        <input type="text" name="description" onChange= {(e)=>setDescription(e.target.value)} value={description}/>
        <button onClick ={insertTutotial}>insert tutotial</button>
        {
          update && <button onClick ={updateTutotial}>update tutotial</button>
        }
      </div>
      <hr />
      <div className="tutotials">
        <h5>List Tutotials</h5>
          <table border="1">
               <thead>
               <tr>
              <th>id</th>
              <th>title</th>
              <th>description</th>
              <th>published</th>
              <th>edit</th>
              <th>delete</th>
            </tr>
               </thead>
              
               <tbody>
               {
                  tutotials.map(({id,title,description,published,created_at,updated_at})=>
                    <tr key={id}>
                        <td>{id}</td>
                        <td>{title}</td>  
                        <td>{description}</td>  
                        <td>{published?"ok":"no"}</td>  
                        <td><button onClick={()=>editTutotial(id)}>Edit</button></td>  
                        <td><button onClick={()=>deleteTutotial(id)}>Remove</button></td>  
                    </tr>
                  )
              
              }
               </tbody>
           
            </table>
      </div>
            
    </div>
  );
}

export default App;
